export const getQuestionsList = async () => {
  try {
    const response = await fetch(`${window.location.origin}/questions.json`);
    return await response.json();
  } catch (e) {
    console.error(e);
  }
};