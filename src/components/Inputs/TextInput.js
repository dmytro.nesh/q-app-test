import PropTypes from 'prop-types';

const TextInput = ({ value, onChange }) => {
  return (
    <div className='border rounded-sm'>
      <input
        className='w-full h-full p-1 bg-transparent text-slate-500 outline-none'
        type='text'
        name='text'
        value={value}
        onChange={onChange}
      />
    </div>
  );
};

TextInput.defaultProps = {
  value: ''
};

TextInput.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

export default TextInput;