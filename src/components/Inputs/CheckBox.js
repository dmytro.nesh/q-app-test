import PropTypes from 'prop-types';

const CheckBox = ({ value, label, onChange }) => {
  return (
    <div>
      <label className='text-slate-500 cursor-pointer'>
        <input
          type='checkbox'
          name={label}
          value={value}
          onChange={onChange}
        />
        <span className='ml-4'>
          {label}
        </span>
      </label>
    </div>
  );
};

CheckBox.defaultProps = {
  value: '',
  label: ''
};

CheckBox.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  onChange: PropTypes.func.isRequired,
};

export default CheckBox;