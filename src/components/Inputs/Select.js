import PropTypes from 'prop-types';

const Select = ({ value, options, onChange }) => {
  return (
    <div className='border rounded-sm'>
      <select
        className='w-full h-full p-1 bg-transparent text-slate-500 outline-none'
        name='select'
        value={value}
        onChange={onChange}
      >
        {options.map((option, index) => (
          <option key={index} value={option.label}>{option.label}</option>
        ))}
      </select>
    </div>
  );
};

Select.defaultProps = {
  value: ''
};

Select.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
  onChange: PropTypes.func.isRequired,
};

export default Select;