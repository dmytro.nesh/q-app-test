const ANSWERS_KEY = 'answers';

export const getAnswers = () => (
  sessionStorage[ANSWERS_KEY] ? JSON.parse(sessionStorage.getItem(ANSWERS_KEY)) : []
);

export const setAnswers = (value) => {
  sessionStorage.setItem(ANSWERS_KEY, JSON.stringify(value) || '');
};