import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import { getAnswers, setAnswers } from '../../store';

const Result = () => {
  const navigate = useNavigate();
  const [answersState, setAnswersState] = useState([]);

  useEffect(() => {
    const answers = getAnswers();
    !answers.length && navigate('/');
    setAnswersState(answers);
  }, []);

  const submitResult = () => {
    // clear store on exit
    setAnswers();
    navigate('/');
  };

  const renderAnswers = (type) => {
    switch (type) {
      case 'checkbox':
        return 'Your multiple selection is:';
      case 'select':
        return 'Your have selected an option:';
      default:
        return 'Your answer is:';
    }
  };

  return (
    <div className='px-4'>
      <div className='md:w-4/6 lg:w-3/6 p-4 mx-auto bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700'>
        {answersState && answersState.map(({ type, value }, index) => {
          const valueData = Array.isArray(value) ? value.join(', ') : value;
          return (
            <div key={index} className='mb-4'>
              <h4 className='my-2 font-bold text-gray-900 dark:text-slate-200'>{renderAnswers(type)}</h4>
              <p className='dark:text-slate-200'>{valueData}</p>
            </div>
          )
        })}
        <div className='mt-8'>
          <button
            className='px-3 py-2 text-gray-900 hover:text-white border border-gray-800 hover:bg-gray-900 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-sm text-center dark:border-gray-600 dark:text-gray-400 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-800'
            onClick={submitResult}
          >
            Okay
          </button>
        </div>
      </div>
    </div>
  );
};

export default Result;