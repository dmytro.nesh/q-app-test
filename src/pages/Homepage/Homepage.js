import { useEffect, useReducer } from 'react';
import { useNavigate } from 'react-router-dom';

import { TextInput, CheckBox, Select } from '../../components';
import { getQuestionsList } from '../../services/api';
import { setAnswers } from '../../store';

const Homepage = () => {
  const navigate = useNavigate();
  const [formData, setFormData] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    {}
  );

  const getQuestionsData = async () => {
    const list = await getQuestionsList();

    const data = list.reduce((obj, item, index) => {
      return Object.assign(obj, {
        [item.type]: {
          ...item,
          isVisible: !index,
          // set default value for select
          ...(item.type === 'select' ? { value: item.inputs[0].options[0].label } : '')
        }
      });
    }, {});

    setFormData(data);
  };

  useEffect(() => {
    // get the data
    getQuestionsData();
  }, []);


  const handleChangeName = (e) => {
    setFormData({ text: { ...formData.text, value: e.target.value }});
  };

  const handleChangeMulti = (e) => {
    const { name } = e.target;

    let newValue = formData.checkbox.value;

    newValue = newValue.includes(name)
      ? newValue.filter(val => val !== name)
      : [...newValue, name];

    setFormData({ checkbox: { ...formData.checkbox, value: newValue }});
  };

  const handleChangeSelect = (e) => {
    setFormData({ select: { ...formData.select, value: e.target.value }});
  };

  // Step by step submission
  const submitName = () => {
    setFormData({
      text: { ...formData.text, isVisible: false },
      checkbox: { ...formData.checkbox, isVisible: true }
    });
  };

  const submitCheckbox = () => {
    setFormData({
      checkbox: { ...formData.checkbox, isVisible: false },
      select: { ...formData.select, isVisible: true }
    });
  };

  const submitSelect = () => {
    const result = Object.values(formData).map(({ type, value }) => ({ type, value }));
    setAnswers(result);
    navigate('/result');
  };

  return (
    <div className='px-4'>
      {formData.text?.isVisible && (
        <div className='md:w-4/6 lg:w-3/6 p-4 mx-auto bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700'>
          <h4 className='my-2 font-bold text-gray-900 dark:text-slate-200'>
            {formData.text.title}
          </h4>
          <TextInput
            value={formData.text.value}
            onChange={handleChangeName}
          />
          <div className='mt-4'>
            <button
              className='px-3 py-2 text-gray-900 hover:text-white border border-gray-800 hover:bg-gray-900 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-sm text-center dark:border-gray-600 dark:text-gray-400 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-800'
              onClick={submitName}
            >
              Next
            </button>
          </div>
        </div>
      )}
      {formData.checkbox?.isVisible && (
        <div className='md:w-4/6 lg:w-3/6 p-4 mx-auto bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700'>
          <h4 className='my-2 font-bold text-gray-900 dark:text-slate-200'>
            {formData.checkbox.title}
          </h4>
          {formData.checkbox?.inputs.map((input, index) => (
            <CheckBox
              key={index}
              value={input.value}
              label={input.label}
              onChange={handleChangeMulti}
            />
          ))}
          <div className='mt-4'>
            <button
              className='px-3 py-2 text-gray-900 hover:text-white border border-gray-800 hover:bg-gray-900 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-sm text-center dark:border-gray-600 dark:text-gray-400 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-800'
              onClick={submitCheckbox}
            >
              Next
            </button>
          </div>
        </div>
      )}
      {formData.select?.isVisible && (
        <div className='md:w-4/6 lg:w-3/6 p-4 mx-auto bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700'>
          <h4 className='my-2 font-bold text-gray-900 dark:text-slate-200'>
            {formData.select.title}
          </h4>
          <Select
            value={formData.select.value}
            options={formData.select.inputs[0].options}
            onChange={handleChangeSelect}
          />
          <div className='mt-4'>
            <button
              className='px-3 py-2 text-gray-900 hover:text-white border border-gray-800 hover:bg-gray-900 focus:ring-4 focus:outline-none focus:ring-gray-300 font-medium rounded-lg text-sm text-center dark:border-gray-600 dark:text-gray-400 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-800'
              onClick={submitSelect}
            >
              Next
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default Homepage;